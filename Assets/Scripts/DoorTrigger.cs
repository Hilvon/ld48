using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorTrigger : MonoBehaviour
{
    public event System.Action OnActivated;
    public void Activate() {
        OnActivated?.Invoke();
    }
}
