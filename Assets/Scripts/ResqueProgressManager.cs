using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResqueProgressManager : MonoBehaviour
{
    [SerializeField]
    private ResqueTarget[] Targets;

    public event System.Action OnAllResqued;
    public int StillMIA => RemainingTargets.Count;

    List<int> RemainingTargets = new List<int>();
    // Start is called before the first frame update
    void Start()
    {
        var i = 0;
        foreach (var miner in Targets) {
            var cur = i;
            RemainingTargets.Add(i);
            miner.OnResqued += () => {
                RemainingTargets.Remove(cur);
                if (RemainingTargets.Count == 0) {
                    OnAllResqued?.Invoke();
                }
            };
        }
    }

}
