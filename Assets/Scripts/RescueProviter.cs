using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RescueProviter : MonoBehaviour
{
    private List<ResqueTarget> _NearbyTargets = new List<ResqueTarget>();

    private void OnTriggerEnter(Collider other) {
        var target = other.GetComponentInParent<ResqueTarget>();
        if (target == null)
            return;
        _NearbyTargets.Add(target);
        TargetsAvailable = _NearbyTargets.Count > 0;
    }

    private void OnTriggerExit(Collider other) {
        var target = other.GetComponentInParent<ResqueTarget>();
        if (target == null)
            return;
        _NearbyTargets.Remove(target);
        TargetsAvailable = _NearbyTargets.Count > 0;
    }

    public bool TargetsAvailable { get; private set; }
    private float ResqueProgress;
    [SerializeField]
    private float RequiredResqueAmount;
    public bool AwaitButtonUp { get; private set; }
    public float RescueFillAmount => ResqueProgress / RequiredResqueAmount;
    

    private void RefreshTargetCount() { 
        
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (AwaitButtonUp && Input.GetKey(KeyCode.E))
            return;
        AwaitButtonUp = false;
        if (!TargetsAvailable) {
            ResqueProgress = 0;
            return;
        }

        if (!Input.GetKey(KeyCode.E)) {
            ResqueProgress = 0;
            return;
        }
        ResqueProgress += Time.deltaTime;
        if (ResqueProgress >= RequiredResqueAmount) {
            ResqueProgress = 0;
            _NearbyTargets[0].Rescue();
            _NearbyTargets.RemoveAt(0);
            TargetsAvailable = _NearbyTargets.Count > 0;
            AwaitButtonUp = true;
        }
    }
}
