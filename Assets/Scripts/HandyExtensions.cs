using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class HandyExtensions
{
    public static int Step(this float A, float B) {
        var tmp = A / B;
        return Mathf.FloorToInt(tmp);// >= 0 ? (int)tmp : ((int)tmp - 1);
    }
}
