using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    [SerializeField]
    private float _MaxSpd;
    [SerializeField]
    private float _Acc;
    [SerializeField]
    private float _Deceleration;
    [SerializeField]
    private CharacterController _Controller;

    [SerializeField]
    private LayerMask _MoveCollisions;
    [SerializeField]
    private Transform _CameraHinge;
    [SerializeField]
    private Camera LookCamera;

    [SerializeField]
    private GameObject ZappEffect;
    [SerializeField]
    private GameObject DigEffect;

    [SerializeField]
    private PotentialSpace _Potentials;
    
    private Vector3 _CurSpeed;
    private float DigGunCharge = 0;
    private bool IsDigCharging;
    [SerializeField]
    private float MaxDigCharge = 3.5f;
    [SerializeField]
    private float DigChargeSpeed = 1.5f;
    [SerializeField]
    private Transform FlashlightTransform;
    [SerializeField]
    private ExitHandler _ExitHandler;
    public float DigChargeRatio => DigGunCharge / MaxDigCharge;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public bool ControlsLock;

    // Update is called once per frame
    void Update()
    {
        
        var targetSpd = ControlsLock?Vector3.zero: transform.rotation * new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical")).normalized*_MaxSpd;
        _CurSpeed = Vector3.MoveTowards(_CurSpeed, targetSpd, (targetSpd.sqrMagnitude> _CurSpeed.sqrMagnitude?_Acc:_Deceleration) * Time.deltaTime);
        //Translate(_CurSpeed*Time.deltaTime);

        //if (_Controller.isGrounded) {
        //    _FallSpeed = 0;
        //}
        //else {
        //    _FallSpeed += 9.8f * Time.deltaTime;
        //    _Controller.Move(Vector3.down * _FallSpeed);
        //}
        _Controller.SimpleMove(_CurSpeed);
        var shotRay = LookCamera.ScreenPointToRay(Input.mousePosition);

        if (Input.GetKey(KeyCode.LeftShift) || _ExitHandler.IsExitWindowOpen || ControlsLock) {
            Cursor.lockState = CursorLockMode.None;// .Locked;
            if (Input.GetKey(KeyCode.LeftShift)) {
                if (Physics.Raycast(shotRay, out var hitInfo, 60)) {
                    FlashlightTransform.rotation = Quaternion.LookRotation(hitInfo.point - FlashlightTransform.position, Vector3.up);
                }
                else {
                    FlashlightTransform.rotation = Quaternion.LookRotation(shotRay.direction, Vector3.up);
                }
            }
        } else {
            FlashlightTransform.localRotation = Quaternion.identity;
            Cursor.lockState = CursorLockMode.Locked;
            transform.Rotate(Vector3.up, Input.GetAxisRaw("Mouse X") * 10);// * Time.deltaTime);
            var camAngles = _CameraHinge.localEulerAngles;
            camAngles = new Vector3(Mathf.Clamp(AngleNearZero(camAngles.x) - Input.GetAxisRaw("Mouse Y") * 10, -45, 45),0,0);
            _CameraHinge.localEulerAngles = camAngles;
        }

        if (Input.GetMouseButtonDown(0)) {
            if (Physics.Raycast(shotRay, out var hitinfo, 50, _MoveCollisions)) {
                Instantiate(ZappEffect, hitinfo.point, Quaternion.identity);
                hitinfo.collider.GetComponent<Zappable>()?.GetZapped();
            }
            // Shoot Zappy thing;
        }
        if (IsDigCharging) {
            DigGunCharge += DigChargeSpeed * Time.deltaTime;
            if (DigGunCharge >= MaxDigCharge)
                DigGunCharge = MaxDigCharge;
        }
        if (Input.GetMouseButtonDown(1)) {
            IsDigCharging = true;
            DigGunCharge = 0;
        }
        if (Input.GetMouseButtonUp(1)) {
            //Shoot Dig gun;
            //Debug.LogError($"Shoot dig gun with {DigGunCharge} charge Direction: {LookCamera.ScreenPointToRay(Input.mousePosition).direction}");
            if (Physics.Raycast(shotRay, out var hitinfo, 50, _MoveCollisions)) {
                Instantiate(DigEffect, hitinfo.point, Quaternion.identity);
                _Potentials.AddDigPoint(hitinfo.point+shotRay.direction * DigGunCharge/4, DigGunCharge / 2);
            }
            else {
                Debug.LogError("No hit...");
            }
            IsDigCharging = false;
            DigGunCharge = 0;
        }
    }

    private float AngleNearZero(float angle) {
        while (angle > 180)
            angle -= 360;
        while (angle < -180)
            angle += 360;
        return angle;
    }

    private void Translate(Vector3 direction) { 
        if (Physics.CapsuleCast(transform.position + Vector3.up*_Controller.radius, transform.position+Vector3.up*(_Controller.height - _Controller.radius), _Controller.radius, direction,out var hitinfo, direction.magnitude, _MoveCollisions)) {
            var travelled = direction.normalized * hitinfo.distance;
            _Controller.Move(travelled); //Traveled however much we could.
            var remainingSpd = direction - travelled;
            remainingSpd = remainingSpd + hitinfo.normal * Vector3.Dot(hitinfo.normal, remainingSpd);
            if (remainingSpd.sqrMagnitude > 0.1f)
                Translate(remainingSpd);
        } else {
            _Controller.Move(direction);
        }
    }

}
