using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulbLighOnZap : MonoBehaviour
{
    [SerializeField]
    private MeshRenderer Renderer;
    [SerializeField]
    private Material LitMaterial;

    public void TirnOn() {
        Renderer.sharedMaterial = LitMaterial;
    }


}
