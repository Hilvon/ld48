using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitHandler : MonoBehaviour
{
    [SerializeField]
    private GameObject OnTryExit;
    public void ConfirmExit() {
        Application.Quit();
    }
    public bool IsExitWindowOpen => OnTryExit.activeInHierarchy;
    
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            OnTryExit.SetActive(true);
        }    
    }
}
