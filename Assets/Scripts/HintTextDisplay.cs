using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class HintTextDisplay : MonoBehaviour
{
    [SerializeField]
    private TMP_Text TextBox;
    [SerializeField]
    private float HintCooldown;
    [SerializeField]
    private GameObject HintPanel;
    [SerializeField]
    private Character _Character;

    private bool WaitForXRelease;
    private float curCD;
    private bool ShowingHint;
    private System.Action _CurCallback;
    public void ShowHint(string text, System.Action callback) {
        SheduledHints.Enqueue((text, callback));
    }

    private Queue<(string, System.Action)> SheduledHints = new Queue<(string, System.Action)>();
    private void Update() {
        if (curCD >0) {
            curCD -= Time.deltaTime;
            return;
        }
        var pressX = Input.GetKey(KeyCode.X);
        if (WaitForXRelease && pressX) {
            return;
        }
        WaitForXRelease = false;
        if (ShowingHint) {
            if (pressX) {
                HintPanel.gameObject.SetActive(false);
                _CurCallback?.Invoke();
                _CurCallback = null;
                curCD = HintCooldown;
                WaitForXRelease = true;
                _Character.ControlsLock = false;
                ShowingHint = false;
            }
        }
        else {
            if (SheduledHints.Count > 0) {
                (TextBox.text, _CurCallback) = SheduledHints.Dequeue();
                HintPanel.gameObject.SetActive(true);
                _Character.ControlsLock = true;
                ShowingHint = true;
            }
        }
    }
}
