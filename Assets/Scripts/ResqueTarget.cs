using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResqueTarget : MonoBehaviour
{
    [SerializeField]
    private GameObject DespawnEffect;
    public event System.Action OnResqued;
    public void Rescue() {
        if (DespawnEffect!= null) Instantiate(DespawnEffect, transform.position, transform.rotation);
        OnResqued?.Invoke();
        Destroy(gameObject);
    }
}
