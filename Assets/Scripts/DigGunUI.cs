using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class DigGunUI : MonoBehaviour
{
    [SerializeField]
    private Character _GunWielder;
    [SerializeField]
    private Image _FillIndicator;
    
    // Update is called once per frame
    void Update()
    {
        _FillIndicator.gameObject.SetActive(_GunWielder.DigChargeRatio >= 0.01f);
        _FillIndicator.fillAmount = _GunWielder.DigChargeRatio;
    }
}
