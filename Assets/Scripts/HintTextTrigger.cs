using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HintTextTrigger : TutorialStep
{
    [SerializeField]
    [TextArea]
    private string Text;

    [SerializeField]
    private HintTextDisplay _Display;

    private bool PlayerInArea;
    private bool Expired;

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        PlayerInArea = false;
        Expired = false;
    }
    
    private void HintClosed() {
        Debug.Log($"{name} Hint closet event callback received.");
        Complete();
    }

    private void OnTriggerEnter(Collider other) {
        if (other.GetComponent<Character>() != null) {
            PlayerInArea = true;
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (PrereqMet && PlayerInArea && !Expired) {
            Debug.Log($"{name} Can display hint");
            _Display.ShowHint(Text, HintClosed);
            Expired = true;
        }
    }
}
