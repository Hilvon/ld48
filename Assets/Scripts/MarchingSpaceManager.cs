using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class MarchingSpaceManager : MonoBehaviour
{
    /// <summary>
    /// This service keeps track of Marching qube volume chunks. Remove unneeded ones, and spawn required ones. 
    /// Also it tracks the changes in the potential space and triggers appropriate volumes to build.
    /// </summary>
    [SerializeField]
    private Vector3 _ChunkSize = Vector3.one * 5;
    [SerializeField]
    private float _cubeStep = 0.1f;
    [SerializeField]
    private PotentialSpace _SpaceLink;
    [SerializeField]
    private MarchingVolume _TemplateVolume;
    [SerializeField]
    private TrackedVolumeRequster[] _TrackedVolumes;
    private int WorldPosToChunkId(Vector3 position) {
        return ChunkPosToChunkId(WorldPosToChunkPos(position));
        //($"x{position.x.Step(_ChunkSize.x)}y{position.y.Step(_ChunkSize.y)}z{position.z.Step(_ChunkSize.z)}").GetHashCode();
    }

    private int ChunkPosToChunkId(Vector3Int chunkPos) {
        return ($"x{chunkPos.x}y{chunkPos.y}z{chunkPos.z}").GetHashCode();
    }

    private Vector3Int WorldPosToChunkPos(Vector3 position) {
        return new Vector3Int(position.x.Step(_ChunkSize.x), position.y.Step(_ChunkSize.y), position.z.Step(_ChunkSize.z));
    }

    private Vector3 ChunkPosToWorldPos(Vector3Int chunkPos) {
        return new Vector3(_ChunkSize.x * chunkPos.x, _ChunkSize.y * chunkPos.y, _ChunkSize.z * chunkPos.z);
    }

    private Dictionary<int, MarchingVolume> _ActiveVolumes = new Dictionary<int, MarchingVolume>();
    private Stack<MarchingVolume> _StashedVolumes = new Stack<MarchingVolume>();
    private Vector3[] _Offsets;
    private IEnumerable<Vector3> AddOffsets(Vector3 origin, float radius) {
        if (_Offsets == null) {
            //Initialize offsets list. Just semi-Hardcode it;
            _Offsets = new Vector3[27];
            _Offsets[0] = Vector3.zero;
            _Offsets[1] = Vector3.left;
            _Offsets[2] = Vector3.right;
            _Offsets[3] = Vector3.up;
            _Offsets[4] = Vector3.down;
            _Offsets[5] = Vector3.forward;
            _Offsets[6] = Vector3.back;
            _Offsets[7] = new Vector3(1, 1, 0).normalized;
            _Offsets[8] = new Vector3(-1, 1, 0).normalized;
            _Offsets[9] = new Vector3(1, -1, 0).normalized;
            _Offsets[10] = new Vector3(-1, -1, 0).normalized;
            _Offsets[11] = new Vector3(1, 0, 1).normalized;
            _Offsets[12] = new Vector3(-1, 0, 1).normalized;
            _Offsets[13] = new Vector3(1, 0, -1).normalized;
            _Offsets[14] = new Vector3(-1, 0, -1).normalized;
            _Offsets[15] = new Vector3(0, 1, 1).normalized;
            _Offsets[16] = new Vector3(0, -1, 1).normalized;
            _Offsets[17] = new Vector3(0, 1, -1).normalized;
            _Offsets[18] = new Vector3(0, -1, -1).normalized;
            _Offsets[19] = new Vector3(1, 1, 1).normalized;
            _Offsets[20] = new Vector3(1, 1, -1).normalized;
            _Offsets[21] = new Vector3(1, -1, 1).normalized;
            _Offsets[22] = new Vector3(1, -1, -1).normalized;
            _Offsets[23] = new Vector3(-1, 1, 1).normalized;
            _Offsets[24] = new Vector3(-1, 1, -1).normalized;
            _Offsets[25] = new Vector3(-1, -1, 1).normalized;
            _Offsets[26] = new Vector3(-1, -1, -1).normalized;
        }
        return _Offsets.Select(_ => origin + _ * radius);
    }

    private MarchingVolume GetVolume(int volumeId) {
        if (_ActiveVolumes.TryGetValue(volumeId, out var volume))
            return volume;
        while (_StashedVolumes.Count > 0) {
            var tryVolume = _StashedVolumes.Pop();
            if (tryVolume != null) {
                tryVolume.gameObject.SetActive(true);
                _ActiveVolumes.Add(volumeId, tryVolume);
                return tryVolume;
            }
        }
        var newVol = Instantiate(_TemplateVolume, transform);
        Vector3Int cubeCount = new Vector3Int(Mathf.CeilToInt(_ChunkSize.x / _cubeStep), Mathf.CeilToInt(_ChunkSize.y / _cubeStep), Mathf.CeilToInt(_ChunkSize.z / _cubeStep));
        Vector3 cubeSize = new Vector3(_ChunkSize.x / cubeCount.x, _ChunkSize.y / cubeCount.y, _ChunkSize.z / cubeCount.z);
        newVol.Init(_SpaceLink, cubeSize, cubeCount);
        _ActiveVolumes.Add(volumeId, newVol);
        return newVol;
    }

    public void PotentialsChanged(Vector3 origin, float radius) {
        foreach (var vol in AddOffsets(origin, radius).Select(WorldPosToChunkId).Distinct().Select(GetVolume)) {
            vol.Rebuild();
        }
    }

    private void Start() {
        _SpaceLink.OnPotentialAdded += PotentialsChanged;
    }
    private void Update() {
        var requiredIds = _TrackedVolumes.SelectMany(_ => RequeriedVolumeIDs(_.Requester.position, _.TrackRadius))
            .Select(_ => (ChunkPosToChunkId(_), _))
            .GroupBy(_ => _.Item1)
            .Select(_ => _.First())
            .ToDictionary(_ => _.Item1, _ => _._);
        var ToKill = _ActiveVolumes.Keys.Except(requiredIds.Keys).ToArray();
        foreach (var ID in ToKill) {
            var vol = _ActiveVolumes[ID];
            vol.Cancel();
            vol.gameObject.SetActive(false);
            _StashedVolumes.Push(vol);
            _ActiveVolumes.Remove(ID);
           // Debug.LogError($"StashingVolume at {WorldPosToChunkPos(vol.transform.position)}");

        }
        foreach (var id in requiredIds.Keys.Except(_ActiveVolumes.Keys)) {
            //Adding new volumes;
            //Debug.LogError($"Initializing new volume at {requiredIds[id]}");
            var addedVolume = GetVolume(id);
            addedVolume.transform.position = ChunkPosToWorldPos(requiredIds[id]);
            addedVolume.Rebuild(true);
        }
    }

    private IEnumerable<Vector3Int> RequeriedVolumeIDs(Vector3 pos, float radius) {
        var NX = WorldPosToChunkPos(pos + Vector3.left * radius);
        var PX = WorldPosToChunkPos(pos + Vector3.right * radius);
        var NY = WorldPosToChunkPos(pos + Vector3.down * radius);
        var PY = WorldPosToChunkPos(pos + Vector3.up * radius);
        var NZ = WorldPosToChunkPos(pos + Vector3.back * radius);
        var PZ = WorldPosToChunkPos(pos + Vector3.forward * radius);


        //var OffsetSteps = new Vector3Int(Mathf.CeilToInt(radius / _ChunkSize.x), Mathf.CeilToInt(radius / _ChunkSize.y), Mathf.CeilToInt(radius / _ChunkSize.z));
        //var Origin = WorldPosToChunkPos(pos);
        var result = new List<Vector3Int>();
        for (int x = NX.x; x <= PX.x; x++) {
            for (int y = NY.y; y <= PY.y; y++) {
                for (int z = NZ.z; z <= PZ.z; z++) {
                    result.Add(new Vector3Int(x, y, z));
                }
            }
        }
        return result;
    }

    [System.Serializable]
    private class TrackedVolumeRequster
    {
        public Transform Requester;
        public float TrackRadius;
    }
}
