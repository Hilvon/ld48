using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TutorialStep : MonoBehaviour
{
    [SerializeField]
    private TutorialStep _Prerequisite;

    public event System.Action OnExpired;
    protected void Complete() {
        OnExpired?.Invoke();
    }

    protected bool PrereqMet;

    protected virtual void Start() {
        PrereqMet = _Prerequisite == null;
        if (_Prerequisite != null) {
            _Prerequisite.OnExpired += PreqMet;
        }
    }

    private void PreqMet() {
        Debug.Log($"{name} Pre-req cloased event received.");
        PrereqMet = true;
    }

}
