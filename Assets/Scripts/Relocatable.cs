using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Relocatable : MonoBehaviour
{
    [SerializeField]
    private Transform _Item;
    [SerializeField]
    private float PositionScale = 1;
    [SerializeField]
    private float RotationScale = 1;
    public void GO() {
        StartCoroutine(MoveItem());
    }

    private IEnumerator MoveItem() {
        while (_Item.localPosition.sqrMagnitude > 0.1f || _Item.localEulerAngles.sqrMagnitude >= 0.1f) {
            _Item.localPosition = Vector3.Lerp(_Item.localPosition, Vector3.zero, Time.deltaTime * PositionScale);
            _Item.localRotation = Quaternion.Lerp(_Item.localRotation, Quaternion.identity, Time.deltaTime * RotationScale);
            yield return null;
        }
    }
}
