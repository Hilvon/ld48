using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPotentialProbeRequest 
{
    Vector3 Origin { get; }
    IEnumerable<Vector3> points { get; }
    event System.Action<IPotentialProbeRequest> OnCancelled;
    void ReturnResult(List<float> result);
}

public interface ICancellable
{
    void Cancel();
}