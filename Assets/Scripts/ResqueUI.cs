using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ResqueUI : MonoBehaviour
{
    [SerializeField]
    private TMPro.TMP_Text Counter;
    [SerializeField]
    private ResqueProgressManager _Manager;
    [SerializeField]
    private GameObject ActivateOnVictory;
    // Start is called before the first frame update
    void Start()
    {
        _Manager.OnAllResqued += SwitchUIMode;
    }

    private void SwitchUIMode() {
        ActivateOnVictory.gameObject.SetActive(true);
        gameObject.SetActive(false);
    }
    // Update is called once per frame
    void Update()
    {
        Counter.text = _Manager.StillMIA.ToString();
    }
}
