using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShortLived : MonoBehaviour
{
    [SerializeField]
    private float Lifetime;
    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, Lifetime);
    }
}
