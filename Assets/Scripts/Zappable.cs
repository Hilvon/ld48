using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class Zappable : MonoBehaviour
{
    [SerializeField]
    private UnityEvent OnZapped;

    public void GetZapped() {
        OnZapped.Invoke();
    }
}
