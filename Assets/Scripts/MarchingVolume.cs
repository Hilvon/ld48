using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Threading;
using System;
//using System.Diagnostics;

public class MarchingVolume : MonoBehaviour, ICancellable
{
    [SerializeField]
    private MeshFilter _DrawnMesh;
    [SerializeField]
    private MeshCollider _Collider;

    private Mesh _MyMeshInstance;
    private PotentialSpace _Space;
    private Vector3 _CubeSize;
    private Vector3Int _CubeCount;
    public void Init(PotentialSpace space, Vector3 cubeSize, Vector3Int cubeCount) {
        _MyMeshInstance = new Mesh();
        _DrawnMesh.sharedMesh = _MyMeshInstance;
        _Collider.sharedMesh = _MyMeshInstance;
        _Collider.convex = false;
        _Collider.isTrigger = false;

        _Space = space;
        _CubeSize = cubeSize;
        _CubeCount = cubeCount;

        //Debug.LogError($"MArching cubes volume initialized with size: {_CubeSize} and count {_CubeCount}");
    }

    private Coroutine _RebuildCoroutine;

    private ConstructionContext _context;

    private System.Action OnCancelled;

    

    public void Rebuild(bool force = false) {
        if (force) { 
            _MyMeshInstance.Clear();
            _MyMeshInstance.UploadMeshData(false);
        }
        if (_RebuildCoroutine != null) {
            if (force) {
                StopCoroutine(_RebuildCoroutine);
                Cancel();
            }
            else
                return; //Already rebuilding;
        }
        _RebuildCoroutine = StartCoroutine(BuiltCoroutine());
        //if (_context == null) {
        //    _context = new ConstructionContext(_CubeSize, _CubeCount);
        //}
        //_context.ReadPotentials(_Space, transform.position);

        //var vertsList = new List<Vector3>();
        //var norms = new List<Vector3>();
        //var tris = new List<int>();
        //for (int x = 0; x < _CubeCount.x; x++)
        //    for (int y = 0; y < _CubeCount.y; y++)
        //        for (int z = 0; z < _CubeCount.z; z++) {
        //            AddCubeToMesh(_context, x, y, z, vertsList, norms, tris);
        //        }
        //_MyMeshInstance.Clear();
        //_MyMeshInstance.vertices = vertsList.ToArray();
        //_MyMeshInstance.normals = norms.ToArray();
        //_MyMeshInstance.triangles = tris.ToArray();
        //_MyMeshInstance.UploadMeshData(false);
    }

    private bool _ThreadFinished;
    private IEnumerator BuiltCoroutine() {
        var tickTimestamp = System.DateTime.UtcNow.Ticks;
        if (_context == null) {
            _context = new ConstructionContext(_CubeSize, _CubeCount);
        }
        _context.Clear();
        //UnityEngine.Debug.Log($"Cleanup phase: {new System.TimeSpan(System.DateTime.UtcNow.Ticks - tickTimestamp).TotalMilliseconds}ms");
        tickTimestamp = System.DateTime.UtcNow.Ticks;
        var wait = true;
        OnCancelled += _context.Cancel;
        _context.ReadPotentials(_Space, transform.position, ()=> { wait = false; });
        yield return new WaitWhile(() => wait);
        OnCancelled -= _context.Cancel;
        //UnityEngine.Debug.Log($"Potentials probed: {new System.TimeSpan(System.DateTime.UtcNow.Ticks - tickTimestamp).TotalMilliseconds}ms");
        tickTimestamp = System.DateTime.UtcNow.Ticks;
        var thread = new Thread(() => { ThreadBody(_context); });
        var killThread = (System.Action)null;
        killThread = () => {
            thread.Abort();
            OnCancelled -= killThread;
        };
        OnCancelled += killThread;
        thread.Start();
        yield return new WaitUntil(() => _context.IsComplete);
        OnCancelled -= killThread;
        //UnityEngine.Debug.Log($"MeshDataGenerated: {new System.TimeSpan(System.DateTime.UtcNow.Ticks - tickTimestamp).TotalMilliseconds}ms");
        tickTimestamp = System.DateTime.UtcNow.Ticks;
        _Collider.enabled = false;
        _MyMeshInstance.Clear();
        _MyMeshInstance.vertices = _context.Verts.ToArray();
        _MyMeshInstance.normals = _context.Normals.ToArray();
        _MyMeshInstance.triangles = _context.Tris.ToArray();
        _MyMeshInstance.UploadMeshData(false);
        _Collider.enabled = true;

        //UnityEngine.Debug.Log($"MeshData Updated: {new System.TimeSpan(System.DateTime.UtcNow.Ticks - tickTimestamp).TotalMilliseconds}ms");
        
        _RebuildCoroutine = null;
    }

    private void ThreadBody(ConstructionContext context) {

        
        //var vertsList = new List<Vector3>();
        //var norms = new List<Vector3>();
        //var tris = new List<int>();
        for (int x = 0; x < _CubeCount.x; x++)
            for (int y = 0; y < _CubeCount.y; y++)
                for (int z = 0; z < _CubeCount.z; z++) {
                    AddCubeToMesh(_context, x, y, z, context.Verts, context.Normals, context.Tris);
                }
        context.IsComplete = true;
    }

    private void AddCubeToMesh(ConstructionContext constructionData, int x, int y, int z, List<Vector3> verts, List<Vector3> norms, List<int> tris) {
        var cubeInfo = constructionData.GetCubePotentialBreaks(new Vector3Int(x, y, z));
        //Debug.LogError($"Redrawing cube {x} {y} {z} resulting potentials: {cubeInfo[0]}{cubeInfo[1]}{cubeInfo[2]}{cubeInfo[3]}{cubeInfo[4]}{cubeInfo[5]}{cubeInfo[6]}{cubeInfo[7]}");
        var trisToAdd = GetCubeTris(cubeInfo);
        int i = 2;
        //Debug.LogError($"Redrawing cube {x} {y} {z} Received tris instructions with {trisToAdd.Count} points");

        while (i < trisToAdd.Count) {
            var curN = verts.Count;
            var p0 = GetVertex(x, y, z, trisToAdd[i - 2], cubeInfo);
            var p1 = GetVertex(x, y, z, trisToAdd[i - 1], cubeInfo);
            var p2 = GetVertex(x, y, z, trisToAdd[i - 0], cubeInfo);
            //Debug.LogError($"Redrawing cube {x} {y} {z} Making a triangle with vertices {p0} {p1} {p2}");
            var normal = new Plane(p0, p1, p2).normal;
            verts.Add(p0);
            verts.Add(p1);
            verts.Add(p2);
            norms.Add(normal);
            norms.Add(normal);
            norms.Add(normal);
            tris.Add(curN);
            tris.Add(curN+1);
            tris.Add(curN+2);
            i += 3;
        }
    }

    private float GetPct(float[] cubeData, int N) {
        var a = 0f;
        var b = 0f;
        switch (N) {
            case 0:
            a = cubeData[0];
            b = cubeData[1];
            break;
            case 1:
            a = cubeData[0];
            b = cubeData[2];
            break;
            case 2:
            a = cubeData[1];
            b = cubeData[3];
            break;
            case 3:
            a = cubeData[2];
            b = cubeData[3];
            break;

            case 4:
            a = cubeData[0];
            b = cubeData[4];
            break;
            case 5:
            a = cubeData[1];
            b = cubeData[5];
            break;
            case 6:
            a = cubeData[2];
            b = cubeData[6];
            break;
            case 7:
            a = cubeData[3];
            b = cubeData[7];
            break;

            case 8:
            a = cubeData[4];
            b = cubeData[5];
            break;
            case 9:
            a = cubeData[5];
            b = cubeData[7];
            break;
            case 10:
            a = cubeData[6];
            b = cubeData[7];
            break;
            case 11:
            a = cubeData[4];
            b = cubeData[6];
            break;
        }
        //Debug.Log($"Determining pct for {N} --  {a} {b}");
        return (a==b)? 0f : (a / (a - b));
    }
    private Vector3 GetVertex(int x, int y, int z, int N, float[] pctData) {
        var a = Vector3.zero;
        var b = Vector3.one;
        var pct = GetPct(pctData, N);
        //Debug.LogError($"Locating vertex position for {x} {y} {z} -- {N} pct = {pct}");
        switch (N) {
            case 0:
            b = new Vector3(_CubeSize.x, 0, 0);
            break;
            case 1:
            b = new Vector3(0, 0, _CubeSize.z);
            break;
            case 2:
            a = new Vector3(_CubeSize.x, 0, 0);
            b = new Vector3(_CubeSize.x, 0, _CubeSize.z);
            break;
            case 3:
            a = new Vector3(0, 0, _CubeSize.z);
            b = new Vector3(_CubeSize.x, 0, _CubeSize.z);
            break;
            case 4:
            b = new Vector3(0,_CubeSize.y, 0);
            break;
            case 5:
            a = new Vector3(_CubeSize.x, 0, 0);
            b = new Vector3(_CubeSize.x, _CubeSize.y, 0);
            break;
            case 6:
            a = new Vector3(0, 0, _CubeSize.z);
            b = new Vector3(0, _CubeSize.y, _CubeSize.z);
            break;
            case 7:
            a = new Vector3(_CubeSize.x, 0, _CubeSize.z);
            b = new Vector3(_CubeSize.x, _CubeSize.y, _CubeSize.z);
            break;

            case 8:
            a = new Vector3(0, _CubeSize.y, 0);
            b = new Vector3(_CubeSize.x, _CubeSize.y, 0);
            break;
            case 9:
            a = new Vector3(_CubeSize.x, _CubeSize.y, 0);
            b = new Vector3(_CubeSize.x, _CubeSize.y, _CubeSize.z);
            break;
            case 10:
            a = new Vector3(0, _CubeSize.y, _CubeSize.z);
            b = new Vector3(_CubeSize.x, _CubeSize.y, _CubeSize.z);
            break;
            case 11:
            a = new Vector3(0, _CubeSize.y, 0);
            b = new Vector3(0, _CubeSize.y, _CubeSize.z);
            break;
        }

        return new Vector3(_CubeSize.x * x, _CubeSize.y * y, _CubeSize.z * z) + Vector3.Lerp(a, b, pct);
    }

    private List<int> GetCubeTris(float[] cornerPotentials) {
        return CubeConstructor.GetCubeTris(cornerPotentials.Select(_ => _ > 0).ToArray());
    }

    public void Cancel() {
        OnCancelled?.Invoke();
    }

    private class ConstructionContext: ICancellable {
        public bool IsComplete;
        public List<Vector3> Verts;
        public List<Vector3> Normals;
        public List<int> Tris;
        
        private Vector3 _cubeSize;
        private Vector3Int _CubeCount;

        private float[] _PointPotentials;
        private Vector3[] _offsets;

        private System.Action OnCancelled;
        public ConstructionContext(Vector3 cubesize, Vector3Int cubecount) {
            _cubeSize = cubesize;
            _CubeCount = cubecount + Vector3Int.one;
            _PointPotentials = new float[(_CubeCount.x) * (_CubeCount.y) * (_CubeCount.z)];
            _offsets = new Vector3[(_CubeCount.x) * (_CubeCount.y) * (_CubeCount.z)];
            for (int x = 0; x < _CubeCount.x; x++)
                for (int y = 0; y < _CubeCount.y; y++)
                    for (int z = 0; z < _CubeCount.z; z++) {
                        _offsets[GetPointIndex(x, y, z)] = ScaledOffset(x, y, z);
                    }
        }

        public void Clear() {
            IsComplete = false;
            Verts = new List<Vector3>();
            Normals = new List<Vector3>();
            Tris = new List<int>();

        }
        public void ReadPotentials(PotentialSpace space, Vector3 origin, System.Action callback) {
            var request = (PotentialSpaceRequest)null;
            var cancel = (System.Action)null;
            cancel = (() => {
                request.Cancel();
                OnCancelled -= cancel;
            });
            request = new PotentialSpaceRequest(origin, _offsets.Select(_ => origin + _), (result) => {
                OnCancelled -= cancel;
                _PointPotentials = result.ToArray();
                callback?.Invoke();
            });
            OnCancelled += cancel;
            space.RequestPotentials(request);//.GetPotentials(_offsets.Select(_ => origin + _), );

            //var swatch = new Stopwatch();
            //swatch.Start();
            //for (int x = 0; x<_CubeCount.x; x++)
            //    for (int y = 0; y<_CubeCount.y; y++)
            //        for (int z = 0; z<_CubeCount.z; z++) {
            //            _PointPotentials[GetPointIndex(x, y, z)] = space.GetPotential(origin + );
            //            //if (swatch.ElapsedMilliseconds >= 10) {
            //                //yield return null;
            //                //swatch.Reset();
            //            //}
            //        }
        }

        private class PotentialSpaceRequest : IPotentialProbeRequest, ICancellable
        {
            Vector3 IPotentialProbeRequest.Origin => _Origin;

            IEnumerable<Vector3> IPotentialProbeRequest.points => _Points;

            public event Action<IPotentialProbeRequest> OnCancelled;

            public void Cancel() {
                OnCancelled?.Invoke(this);
            }

            private Vector3 _Origin;
            private IEnumerable<Vector3> _Points;
            private Action<List<float>> _ResponseCallback;

            public PotentialSpaceRequest(Vector3 origin, IEnumerable<Vector3> points, Action<List<float>> responseCallback) {
                this._Origin = origin;
                this._Points = points;
                this._ResponseCallback = responseCallback;
            }

            void IPotentialProbeRequest.ReturnResult(List<float> result) {
                _ResponseCallback?.Invoke(result);
            }
        }
        public float[] GetCubePotentialBreaks(Vector3Int cubeIndex) {
            return new float[8] {
                _PointPotentials[GetPointIndex(cubeIndex + new Vector3Int(0,0,0))],
                _PointPotentials[GetPointIndex(cubeIndex + new Vector3Int(1,0,0))],
                _PointPotentials[GetPointIndex(cubeIndex + new Vector3Int(0,0,1))],
                _PointPotentials[GetPointIndex(cubeIndex + new Vector3Int(1,0,1))],
                _PointPotentials[GetPointIndex(cubeIndex + new Vector3Int(0,1,0))],
                _PointPotentials[GetPointIndex(cubeIndex + new Vector3Int(1,1,0))],
                _PointPotentials[GetPointIndex(cubeIndex + new Vector3Int(0,1,1))],
                _PointPotentials[GetPointIndex(cubeIndex + new Vector3Int(1,1,1))]
            };
        }

        private float GetPotentialDiff(Vector3Int A, Vector3Int B) {
            var pA = _PointPotentials[GetPointIndex(A)];
            var pB = _PointPotentials[GetPointIndex(B)];

            var t = (pA == pB)? 0
                : pA == 0 ? 0 : (pA - pB) / pA;
            if (pB > pA) t = -t;
            return t;
        }

        public Vector3 ScaledOffset(int x, int y, int z) {
            return new Vector3(_cubeSize.z * x, _cubeSize.y * y, _cubeSize.z * z);
        }

        private int GetPointIndex(Vector3Int pointPos) {
            return GetPointIndex(pointPos.x, pointPos.y, pointPos.z);
        }
        private int GetPointIndex(int x, int y, int z) {
            if (x > _CubeCount.x || y > _CubeCount.y || z > _CubeCount.z)
                throw new System.Exception("ERROR! Requesting index of a point outside of cubecounts");
            return x + _CubeCount.x * (y + _CubeCount.y * z);
        }

        public void Cancel() {
            OnCancelled?.Invoke();
        }
    }
}
