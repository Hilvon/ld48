using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanRescueUI : MonoBehaviour
{
    [SerializeField]
    private RescueProviter _Provider;
    [SerializeField]
    private GameObject HintPanel;
    [SerializeField]
    private Image ProgressFill;

    private void Update() {
        HintPanel.SetActive(_Provider.TargetsAvailable);
        ProgressFill.fillAmount = _Provider.RescueFillAmount;
    }
}
