using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MArchingVolumeTester : MonoBehaviour
{
    [SerializeField]
    private MarchingVolume _Subject;
    [SerializeField]
    private Vector3 _ChunkSize = Vector3.one * 5;
    [SerializeField]
    private float _cubeStep = 0.1f;
    [SerializeField]
    private PotentialSpace _SpaceLink;

    private void Start() {
        Vector3Int cubeCount = new Vector3Int(Mathf.FloorToInt(_ChunkSize.x / _cubeStep), Mathf.FloorToInt(_ChunkSize.y / _cubeStep), Mathf.FloorToInt(_ChunkSize.z / _cubeStep));
        Vector3 cubeSize = new Vector3(_ChunkSize.x / cubeCount.x, _ChunkSize.y / cubeCount.y, _ChunkSize.z / cubeCount.z);
        _Subject.Init(_SpaceLink, cubeSize, cubeCount);
    }
    private void Update() {
        _Subject.Rebuild();
    }
}
