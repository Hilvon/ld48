using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Door : MonoBehaviour
{
    [SerializeField]
    private List<DoorTrigger> _Triggers;
    [SerializeField]
    private UnityEvent _OnAllTriggered;
    // Start is called before the first frame update
    void Start()
    {
        foreach (var trigger in _Triggers) {
            var trg = trigger;
            trg.OnActivated += () => {
                _Triggers.Remove(trg);
                if (_Triggers.Count == 0)
                    _OnAllTriggered?.Invoke();
            };
        }
    }
}
