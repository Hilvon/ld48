using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Diagnostics;

public class PotentialSpace : MonoBehaviour
{
    public float GetPotential(Vector3 worldPos) {
        //Debug.LogError($"Potential is being wuried at {worldPos}");
        if (_FixedColliders.Any(_ => _.gameObject.activeInHierarchy && _.bounds.Contains(worldPos) && _.ClosestPoint(worldPos) == worldPos))
            return -1;
        var PointsNearby = _DIggedPoints.Where(_ => (_.position - worldPos).sqrMagnitude <= _.radius * _.radius + 1 + 2 * _.radius).ToList();
        return PointsNearby.Count>0? PointsNearby.Min(_ => (_.position - worldPos).magnitude - _.radius):1;
        //    return -1;
        //return 1;
    }

    //public void GetPotentials(IEnumerable<Vector3> points, System.Action<List<float>> callback) {
    //    //_Sheduled.Enqueue((callback, points));
    //    var samplePoint = points.First();
    //    //if (_Pending.ContainsKey(points.First()))
    //    _Pending[samplePoint] = (callback, points);
    //}

    private Queue<(System.Action<List<float>>, IEnumerable<Vector3>)> _Sheduled = new Queue<(System.Action<List<float>>, IEnumerable<Vector3>)>();
    private Dictionary<Vector3, (System.Action<List<float>>, IEnumerable<Vector3>)> _Pending = new Dictionary<Vector3, (System.Action<List<float>>, IEnumerable<Vector3>)>();

    private Dictionary<Vector3, IPotentialProbeRequest> _PendingRequests = new Dictionary<Vector3, IPotentialProbeRequest>();

    public void RequestPotentials(IPotentialProbeRequest request) {
        //UnityEngine.Debug.LogError($"Got request for {request.Origin}");
        if (_PendingRequests.ContainsKey(request.Origin)) {
            UnityEngine.Debug.LogError($"Clashed requests for {request.Origin}. The former request should have been cancelled.");
            (_PendingRequests[request.Origin] as ICancellable)?.Cancel();
        }
        _PendingRequests[request.Origin] = request;
        request.OnCancelled += UnpendRequest;
    }

    private void UnpendRequest(IPotentialProbeRequest request) {
        _PendingRequests.Remove(request.Origin);
        request.OnCancelled -= UnpendRequest;
    }

        private IEnumerator RunPotentials() {
        var curCB = (System.Action <List<float>>)null;
        var curIn = (IEnumerable<Vector3>)null;
        var curRequest = (IPotentialProbeRequest)null;
        var curOrigin = Vector3.zero;
        while (true) {
            if (curRequest == null) {
                //if (_Sheduled.Count > 0) {
                //    (curCB, curIn) = _Sheduled.Dequeue();
                //}
                if (_PendingRequests.Count > 0) {
                    curOrigin = _PendingRequests.Keys.OrderBy(_ => (_ - _MainCharacter.position).sqrMagnitude).FirstOrDefault();
                    // (curCB, curIn) = _PendingRequests[curOrigin];
                    curRequest = _PendingRequests[curOrigin];
                    curRequest.OnCancelled -= UnpendRequest;
                    _PendingRequests.Remove(curOrigin);
                }
            }
            if (curRequest != null) {
                //UnityEngine.Debug.LogError($"Starting processing request for {curRequest.Origin}");
                var STOP = false;
                var stopper = (System.Action<IPotentialProbeRequest>)((r) => { STOP = true; });
                curRequest.OnCancelled += stopper;
                var startTick = System.DateTime.UtcNow.Ticks;
                var folds = 0;
                var steps = 5000;
                var cache = GetCachedVolume(curRequest.Origin, curRequest.points);
                var curOut = new List<float>();
                cache.Readout();
                if (cache.IsEmpty) {
                    //UnityEngine.Debug.LogError("The area looks like it is empty. FastTracking...");
                    curOut = curRequest.points.Select(_ => 1f).ToList();
                }
                else {
                    foreach (var point in curRequest.points) {
                        curOut.Add(cache.GetPointPotential(point));// GetPotential(point));
                        steps--;
                        if (steps < 0) {
                            folds++;
                            yield return null;
                            steps = 5000;
                        }
                        if (STOP)
                            break;
                    }
                }
                curRequest.OnCancelled -= stopper;
                if (!STOP) {
                    //UnityEngine.Debug.LogError($"Processing potential volume took {new System.TimeSpan(System.DateTime.UtcNow.Ticks - startTick).TotalMilliseconds}ms with {folds} folds");
                    curCB?.Invoke(curOut);
                    curRequest.ReturnResult(curOut);
                }
                curRequest = null;
            }
            //UnityEngine.Debug.LogError($"Completed processing a potential measurements request. {_Sheduled.Count} requests remaining");
            yield return null;
        }
    }

    private CachedVolumeData GetCachedVolume(Vector3 origin, IEnumerable<Vector3> allPoints) { 
        if (_Caches.TryGetValue(origin, out var cache)) {
            return cache;
        }
        var newBounds = new Bounds(origin,Vector3.one*0.1f);
        foreach (var pt in allPoints) {
            newBounds.Encapsulate(pt);
        }
        var newCache = new CachedVolumeData(newBounds, _FixedColliders, _DIggedPoints);
        _Caches.Add(origin, newCache);
        return newCache;
    }

    [SerializeField]
    private Collider[] _FixedColliders;
    [SerializeField]
    private Transform _MainCharacter;

    public event System.Action<Vector3, float> OnPotentialAdded;

    private List<DigPoint> _DIggedPoints = new List<DigPoint>();

    private class CachedVolumeData {
        public Collider[] RelevantColliders;
        public List<DigPoint> RelevantDigPoints;
        public Bounds Bounds;

        public float GetPointPotential(Vector3 worldPos) {
            if (IsEmpty)
                return 1;

            if (RelevantColliders.Any(_ => _.gameObject.activeInHierarchy && _.bounds.Contains(worldPos) && _.ClosestPoint(worldPos) == worldPos))
                return -1;
            var PointsNearby = RelevantDigPoints.Where(_ => (_.position - worldPos).sqrMagnitude <= _.radius * _.radius + 1 + 2 * _.radius).ToList();
            return PointsNearby.Count > 0 ? PointsNearby.Min(_ => (_.position - worldPos).magnitude - _.radius) : 1;

        }
        public bool IsEmpty => RelevantColliders.Length == 0 && RelevantDigPoints.Count == 0;
        public void Readout() {
            //UnityEngine.Debug.LogError($"Volume summary: {Bounds.center} {RelevantColliders.Length} {RelevantDigPoints.Count}");
        }
        public CachedVolumeData(Bounds bounds, Collider[] allColliders, List<DigPoint> allPoints) {
            Bounds = bounds;
            RelevantColliders = allColliders.Where(_ => _.bounds.Intersects(bounds)).ToArray();
            RelevantDigPoints = allPoints.Where(_ => bounds.SqrDistance(_.position) <= _.radius * _.radius + 2 * _.radius + 1).ToList();
        }
    }

    private Dictionary<Vector3, CachedVolumeData> _Caches = new Dictionary<Vector3, CachedVolumeData>();

    private class DigPoint
    {
        public Vector3 position { get; private set; }
        public float radius { get; private set; }
        private List<CachedVolumeData> _Mentions;
        public DigPoint(Vector3 position, float radius) {
            this.position = position;
            this.radius = radius;
            _Mentions = new List<CachedVolumeData>();
        }
        public void AddMention(CachedVolumeData cache) {
            _Mentions.Add(cache);
            cache.RelevantDigPoints.Add(this);
        }
        public void ClearMentions() {
            foreach (var mention in _Mentions) {
                mention.RelevantDigPoints.Remove(this);
            }
        }
    }

    List<int> _ToKill = new List<int>();
    public void AddDigPoint(Vector3 position, float radius) {
        //UnityEngine.Debug.LogError($"Receiving nex potential point at {position} radius {radius}");
        _ToKill.Clear();
        for (int i = 0; i < _DIggedPoints.Count; i++) {
            var cur = _DIggedPoints[i];
            if ((cur.position - position).sqrMagnitude <= cur.radius * cur.radius + radius * radius - 2 * cur.radius * radius) {
                if (cur.radius > radius)
                    return; //This dig point is already encompassed by another existing point;
                else
                    _ToKill.Add(i);
            }   
        }
        foreach (var i in _ToKill) {
            _DIggedPoints[i].ClearMentions();
            _DIggedPoints.RemoveAt(i);
        }
        var newPoint = new DigPoint(position, radius);
        _DIggedPoints.Add(newPoint);
        foreach (var c in _Caches.Values.Where(_ => _.Bounds.SqrDistance(position) <= radius * radius + 2 * radius + 1)) {
            newPoint.AddMention(c);
        }
        OnPotentialAdded?.Invoke(position, radius+1);
    }
    // Start is called before the first frame update
    void Start() {
        StartCoroutine(RunPotentials());
    }

    // Update is called once per frame
    void Update() {

    }
}
